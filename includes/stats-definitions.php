<?php

require_once __DIR__ . '/stats-sql.php';

$charts = [
    0 => [
        0 => [
            'title' => 'Average number of pages patrolled per day',
            'id' => 'pagesReviewed'
        ],
        1 => [
            'title' => 'Active reviewers',
            'id' => 'activeReviewers'
        ]
    ],
    1 => [
        0 => [
            'title' => 'Average number of page curation actions per day',
            'id' => 'curationActions'
        ],
        1 => [
            'title' => 'NPP Backlog',
            'id' => 'NPPBacklog'
        ]
    ],
    2 => [
        0 => [
            'title' => 'NPP Backlog - fraction of articles created by non-autoconfirmed users',
            'id' => 'NPPBacklogNAutoconfirmedFraction'
        ],
        1 => [
            'title' => 'NPP Backlog - median edit count of the creator',
            'id' => 'NPPBacklogMedianEdits'
        ]
    ],
    3 => [
        0 => [
            'title' => 'Average number of articles deleted per day (mainspace only, redirects excluded where possible, rough numbers)',
            'id' => 'Deletion'
        ],
        1 => [
            'title' => 'Placeholder',
            'id' => 'skip'
        ]
    ],
    4 => [
        0 => [
            'title' => 'CSD Breakdown, per day (mainspace only, redirects excluded where possible, rough numbers)',
            'id' => 'DeletionCSDBreakdown'
        ]
    ]
];

function prepareData($id)
{
    global $dataSources;

    $spec = is_callable($dataSources[$id]) ? $dataSources[$id]() : $dataSources[$id];

    $data = getData($spec);
    array_unshift($data, $spec['legend']);

    return $data;
}

function getKey($ym, $axisField) {
    if ($axisField === 'yearmonth') {
        $year = substr($ym, 2, 2);
        $month = substr($ym, 4);

        $key = [
                '01' => 'Jan',
                '02' => 'Feb',
                '03' => 'Mar',
                '04' => 'Apr',
                '05' => 'May',
                '06' => 'Jun',
                '07' => 'Jul',
                '08' => 'Aug',
                '09' => 'Sep',
                '10' => 'Oct',
                '11' => 'Nov',
                '12' => 'Dec'
            ][$month] . ' ' . $year;
    } else {
        $key = $ym;
    }

    return $key;
}

function formatKey($key, $axisField) {
    if ($axisField === 'ts') {
        $key = date('M d', strtotime($key));
    }

    return $key;
}

function getData($spec) {
    global $db;

    $sql = $spec['sql'];
    $id = $spec['id'];
    $axis = $spec['legend'][0];

    $result = [];

    if (!is_array($sql)) {
        $sql = [ $sql ];
    }

    $axisField = [
        'Month' => 'yearmonth',
        'Timestamp' => 'ts'
    ][$axis];

    foreach ($sql as $no => $query) {
        $cacheKey = md5($query);

        if ($data = $db->fetchOne('SELECT `data` FROM sqlcache WHERE `key`=' . $db->quote($cacheKey))) {
            $data = unserialize($data);
        } else {
            $data = $db->fetchAll($query);
            $db->insert('sqlcache', [
                'key' => $cacheKey,
                'data' => serialize($data),
                'name' => $id
            ]);
        }

        foreach ($data as $r) {
            $key = getKey($r[$axisField], $axisField);
            unset($r[$axisField]);

            if (!isset($result[$key])) {
                $result[$key] = [ formatKey($key, $axisField) ];
            }

            foreach ($r as $v) {
                $value = is_numeric($v) ? (floor($v)==$v ? (int)$v : (float)round($v,2)) : $v;

                if (isset($spec['average']) && $spec['average'] === 'daily') {
                    if ($key === date('M y')) {
                        $days = date('j');
                    } else {
                        $arr = explode(' ', $key);
                        $days = cal_days_in_month(CAL_GREGORIAN, [
                            'Jan' => 1,
                            'Feb' => 2,
                            'Mar' => 3,
                            'Apr' => 4,
                            'May' => 5,
                            'Jun' => 6,
                            'Jul' => 7,
                            'Aug' => 8,
                            'Sep' => 9,
                            'Oct' => 10,
                            'Nov' => 11,
                            'Dec' => 12
                        ][$arr[0]] , '20' . (string)$arr[1]);
                    }

                    $value = round($value / $days);
                }

                $result[$key][] = $value;
            }
        }
    }

    return  array_values($result);
}

