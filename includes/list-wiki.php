<?php

$result = getArticlesByGroups($groups, $mode);

?>

{{Warning|This is an automatically generated list. Do not edit manually — all changes will be lost on the next update.}}

Unreviewed articles by keyword - a quick & dirty list. There are bound to be many false positives and negatives.

<?php

foreach ($result as $category => $byKeyword) {
    $cnt = 0;

    foreach($byKeyword as $articles) {
        $cnt += count($articles);
    }

    echo '== ' . $category . ' (' . $cnt . ') == ' . "\n";

    foreach ($byKeyword as $keyword => $articles)
    {
        echo "<div style=\"margin-bottom:16px;\" class=\"toccolours mw-collapsible mw-collapsed\">\n";

        echo '===' . $keyword . ' (' . count($articles) . ')===' . "\n";
        echo "<div class=\"mw-collapsible-content\">\n";

        foreach ($articles as $a) {
            echo '# [[' . $a['title'] . ']]' . "\n";
        }

        echo "</div>";

        echo "</div>";
        echo "\n\n";
    }
}



