<?php

require_once __DIR__ . '/common.php';
//require_once __DIR__ . '/npp-config.php';
//require_once __DIR__ . '/npp-downloader.php';


$data = json_decode(file_get_contents(__DIR__ . '/data/feed3.json'), true);

$cnt = 0;

foreach($data as $d) {
    if ($d['linkcount'] > 0) {

        print_r($d);

        if ($cnt++ > 10) {
            die();
        }
    }
}



die('aa');
print_r(array_slice($data,0,5));die();

//str_replace . , [[


$result = [];



ksort($keywords);

$total = 0;

foreach ($keywords as $keyword => $category) {
    $result[$keyword] = [
        'count' => 1,
        'items' => []
    ];

    foreach ($data as $r) {
        //if (in_array(str_replace('_',' ', strtolower($r['title'])), $versity)) {
        if (stripos($r['title'], $keyword) !== false || stripos($r['snippet'], $keyword) !== false) {
            $result[$keyword]['count']++;
            $result[$keyword]['items'][] = '# [[' . $r['title'] . ']]';
        }
    }

    sort($result[$keyword]['items']);

    $total += $result[$keyword]['count'];
    //echo $keyword . ': ' . $result[$keyword]['count'] . "\n";
}

//echo 'TOTAL: ' . $total . "\n\n";

$none = [];

foreach ($data as $r) {
    foreach ($keywords as $k => $cat) {
        if (stripos($r['snippet'], $k) !== false || stripos($r['title'], $k) !== false) {
            continue 2;
        }
    }

    $none[] = '# [[' . $r['title'] . ']]';//. ' <small>' . htmlentities(str_replace(["\n", "\r"], " ", substr($r['snippet'], 0, 100))) . '</small>';
}

foreach ($result as $k => &$r) {
    ksort ($r);
} unset($r);

$result['(no keyword matches)'] = [
    'count' => count($none),
    'items' =>  $none
];

//echo 'NONE: ' . count($none) . "\n\n";

echo "{{Warning|This is an automatically generated list. Do not edit manually — all changes will be lost on the next update.}}\n\n";

echo "Unreviewed articles by keyword - a quick & dirty list. There are bound to be many false positives and negatives, and overlap, but all unreviewed articles are included.\n";

$byCategory = [];
foreach ($keywords as $k => $cat) {
    if(!isset($byCategory[$cat])) {
        $byCategory[$cat] = [];
    }

    $byCategory[$cat][$k] = $result[$k];
}

$misc = $byCategory['Miscellaneous'];
unset($byCategory['Miscellaneous']);
$byCategory['Miscellaneous'] = $misc;

foreach ($byCategory as $category => $result) {
    $cnt = 0;

    foreach($result as $r) {
        $cnt += $r['count'];
    }

    echo '== ' . $category . ' (' . $cnt . ') == ' . "\n";

    foreach ($result as $keyword => $r)
    {
        echo "<div style=\"margin-bottom:16px;\" class=\"toccolours mw-collapsible mw-collapsed\">\n";

        echo '===' . $keyword . ' (' . $r['count'] . ')===' . "\n";
            echo "<div class=\"mw-collapsible-content\">\n";
            echo implode("\n", $r['items']);
            echo "</div>";

        echo "</div>";
        echo "\n\n";
    }
}

die();


usort($data, function($a, $b){
    return strnatcmp($a['page_len'], $b['page_len']);
});

for($i=0;$i<20000;$i++) {
    $keyword = 'naval';

    if (stripos($data[$i]['snippet'], $keyword) !== false || stripos($data[$i]['title'], $keyword) !== false) {
        echo '<a href="https://en.wikipedia.org/wiki/' . ($data[$i]['title'])   . "\"></a> " . /*$data[$i]['page_len'] . */" " .    str_replace(["\n","\r"], " ", $data[$i]['snippet']) . "\n";
    }
}

die();

$byUserArts = [];
$byUser = [];
foreach ($data as $r) {
    if(!isset($byUser[$r['user_name']])) {
        $byUser[$r['user_name']] = 0;
    }

    if(!isset($byUserArts[$r['user_name']])) {
        $byUserArts[$r['user_name']] = [];
    }

    $byUserArts[$r['user_name']][] = $r;

    $byUser[$r['user_name']]++;
}

$byUserArtsOld = [];
$byUserOld = [];
foreach ($data as $r) {
    if(!isset($byUserOld[$r['user_name']])) {
        $byUserOld[$r['user_name']] = 0;
    }

    if(!isset($byUserArtsOld[$r['user_name']])) {
        $byUserArtsOld[$r['user_name']] = [];
    }

    $byUserArtsOld[$r['user_name']][] = $r;

    $byUserOld[$r['user_name']]++;
}

arsort($byUser);

$moreThan = [
];

$moreThanOld = [
];

foreach ([50, 20, 10, 5, 2, 0] as $n) {
    $moreThan[$n] = [
        'count' => 0,
        'editors' => []
    ];

    $moreThanOld[$n] = [
        'count' => 0,
        'editors' => []
    ];
}

foreach ($byUser as $u => $count) {
    foreach ($moreThan as $threshold => &$table) {
        if ($count > $threshold) {
            $table['count'] += $count;
            $table['editors'][] = $u;
        }
    } unset($table);

    foreach ($moreThanOld as $threshold => &$table) {
        if ($count > $threshold) {
            $table['count'] += $count;
            $table['editors'][] = $u;
        }
    } unset($table);
}

echo "{| class='wikitable'
    |-
    ! Unreviewed Articles 
    ! Editors
    ! Articles
    ! Backlog fraction    
    ";


foreach ($moreThan as $threshold => $table) {
    echo "|-\n";

    if ($threshold == 0 ) {
        echo "| -  \n";
    } else {
        echo "| >" . $threshold . "\n";
    }

    echo "|" . count($table['editors']) . "\n";
    echo "|" . $table['count'] . "\n";
    echo "|" . round(($table['count']/count($data))*100) . "% \n";
}

echo "|- \n";
echo "| colspan=\"4\" | " . "<small>Generated: " . date('Y-m-d H:i:s', strtotime($data[0]['creation_date'])) . " UTC</small>\n";

echo "|}\n\n";

echo "== Editors with most unreviewed articles ==\n\n";

echo "{| class='wikitable sortable'
    |-
    ! Editor
    ! Articles in backlog
    ! Avg page length
    ! Contributions (new articles)
    ! Random sample
    ";

foreach ($byUser as $u => $count) {
    if ($count <= 5) {
        continue;
    }

    echo "|-\n";

    echo "| [[User:" . htmlentities($u) . '|' . htmlentities($u) . ']]' . ' ([[User_talk:' . htmlentities($u) . '|talk]])' . "\n";
    echo "| $count \n";

    $lengths = [];

    foreach ($byUserArts[$u] as $art) {
        $lengths[] = $art['page_len'];
    }

    echo "| " . round(array_sum($lengths)/count($lengths)) . "\n";

    echo '| [' . 'https://en.wikipedia.org/w/index.php?limit=50&title=Special:Contributions&contribs=user&target=' . urlencode($u) . '&namespace=0&tagfilter=&newOnly=1&start=&end=' . ' contributions] ' . "\n";


    $sample = [];
    while (!empty($byUserArts[$u]) && count($sample) < 3) {
        $key = array_rand($byUserArts[$u]);
        $sample[] = $byUserArts[$u][$key];
        unset($byUserArts[$u][$key]);
    }

    echo "|\n";

    foreach ($sample as $s) {
        echo '* <small>[[' . htmlentities($s['title']) . ']] ' . str_replace(["\n","\r"], " ", $s['snippet']) . '</small>' . "\n";
    }
}

echo "|}";