<?php

require_once __DIR__ . '/../common.php';
require_once __DIR__ . '/../includes/stats-definitions.php';

$id = $_GET['id'];

if (!isset($dataSources[$id])) {
    die('INVALID');
}

$data = prepareData($id);

$fh = fopen('php://temp', 'rw'); # don't create a file, attempt

foreach ( $data as $row ) {
    fputcsv($fh, $row);
}
rewind($fh);
$csv = stream_get_contents($fh);
fclose($fh);

header("Content-type: text/csv");
header("Content-Disposition: attachment; filename=$id.csv");
header("Pragma: no-cache");
header("Expires: 0");
echo $csv;
