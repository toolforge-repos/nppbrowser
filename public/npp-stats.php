<?php

require_once __DIR__ . '/../common.php';
require_once __DIR__ . '/../includes/stats-definitions.php';

?>
<html>
<head>
    <!-- Bootstrap Core CSS -->
    <link href="./vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="./vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="./vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="./vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="./dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="./vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="./vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <script type="text/javascript">
        google.charts.load('current', {'packages':['corechart']});
    </script>
</head>
<body style="padding:5px">

<div id="#page-wrapper">
    <h1>Statistics</h1>
    <?php foreach($charts as $row => $ch): ?>
        <div class="row">
            <?php foreach($ch as $col => $chart): ?>
            <?php if ($chart['id'] === 'skip') continue; ?>
            <?php if (count($ch) === 2): ?>
            <div class="col-lg-6">
            <?php else: ?>
            <div class="col-lg-12">
            <?php endif; ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <span style="color:#000"><?= $chart['title']; ?></span>

                        <div class="pull-right">
                            <div class="btn-group">
                                <a class="btn btn-default btn-xs" href="./npp-stats-export.php?id=<?= $chart['id'];?>">
                                    Export CSV
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">

                        <div id="chart_<?= $row;?>_<?= $col; ?>" style="width: <?= 1560*(1/count($ch)); ?>px; height: 500px"></div>
                        <script type="text/javascript">
                            google.charts.setOnLoadCallback(function() {
                                var data = google.visualization.arrayToDataTable(<?= json_encode(prepareData($chart['id'])); ?>)

                                var options = {
                                    //title: 'Active reviewers',
                                    legend: { position: 'bottom' }
                                };

                                <?php if($chart['id'] === 'DeletionCSDBreakdown'): ?>
                                    options['isStacked'] = true;
                                    var chart = new google.visualization.BarChart(document.getElementById('chart_<?= $row; ?>_<?= $col; ?>'));
                                <?php else: ?>
                                    var chart = new google.visualization.LineChart(document.getElementById('chart_<?= $row; ?>_<?= $col; ?>'));
                                <?php endif; ?>

                                chart.draw(data, options);
                            });
                        </script>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    <?php endforeach; ?>
</div>

</body>
</html>