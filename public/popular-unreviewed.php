<?php

require_once __DIR__ . '/../common.php';

$db1 = new mysqli( $dbHost, $dbUser, $dbPass, $dbName );
$result = $db1->query( "select title, snippet, pageviews from article join npp_pageviews on article.wiki_id=npp_pageviews.pageid where article.redirect='0' and patrol_status='0' order by pageviews desc limit 20" );
$pages = $result->fetch_all(MYSQLI_ASSOC);
?>
<html>
<head>
    <!-- Bootstrap Core CSS -->
    <link href="./vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="./vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="./vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="./vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="./dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="./vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="./vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<style>
td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
</head>
<body>
<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="./">NPP Browser <small style="font-size:0.5em">v. 1.3.0</small></a>
    </div>
    <!-- /.navbar-header -->

    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <?php require '../includes/menu.php'; ?>
            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
</nav>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-body">
					<h3>Popular unreviewed articles</h3>
					<table>
						<tr><th>Page</th><th>Daily pageviews</th></tr>
					<?php
						foreach ($pages as $page) {
					?>
						<tr><td><a title="<?=$page['snippet']?>" target="_blank" href="https://en.wikipedia.org/wiki/<?=htmlentities($page['title'])?>"><?=$page['title']?></a></td>
						<td><?=$page['pageviews']?></td></tr>
					<?php
					}
					?>
					</table>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
