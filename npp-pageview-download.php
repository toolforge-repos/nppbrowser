<?php
require_once __DIR__ . '/common.php';

echo "---\nBatch started at: ".date('c')."\n";
$batchSize = 100;
$db1 = new mysqli( $dbHost, $dbUser, $dbPass, $dbName );

deleteOlderEntriesInPageViewTable($db1);

$result = $db1->query( "select wiki_id, title, creation_date from article where patrol_status=0 and redirect=0 and wiki_id not in (select pageid from npp_pageviews) and creation_date < date_add(now(), interval -2 day) order by creation_date limit $batchSize" );
$pages = $result->fetch_all(MYSQLI_ASSOC);
echo sizeof($pages)." new pages to be updated...\n";

// If there aren't enough new pages, then update data for some of the older pages
if( sizeof($pages) < $batchSize ) {
	$queryLimit = $batchSize - sizeof($pages);
	$result = $db1->query( "select wiki_id, title, creation_date from article join npp_pageviews on pageid=wiki_id where patrol_status=0 and article.redirect=0 and updated_on < date_add(now(), interval -2 day) order by updated_on desc limit $queryLimit" );
	$pages1 = $result->fetch_all(MYSQLI_ASSOC);
	echo sizeof($pages1)." old pages to be updated...\n";
	$pages = array_merge($pages, $pages1);
}

//Get the pageview data for each of them
foreach ($pages as $page) {
	$avgPageViews = getAvgDailyPageView($page['title']);
	$result = $db1->query( "insert into npp_pageviews (pageid, pageviews) values(".$page['wiki_id'].", ".$avgPageViews." ) on duplicate key update pageviews = ".$avgPageViews);
}

$totalTime = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];
echo "Done - time taken: ".round($totalTime, 2) . " s (" . round($totalTime/60, 2) . " min)\n";

function deleteOlderEntriesInPageViewTable($db, $daysBack = 10) {
	$result = $db->query( "delete from npp_pageviews where updated_on < date_add(now(), interval -$daysBack day)" );
}

function getAvgDailyPageView($wikiTitle) {
	$daysBack = 10;
	$startDate = date("Ymd", mktime(0, 0, 0, date("m"), date("d")-2-$daysBack, date("Y") ) );
	$endDate = date("Ymd", mktime(0, 0, 0, date("m"), date("d")-2, date("Y") ) );
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$url = "https://wikimedia.org/api/rest_v1/metrics/pageviews/per-article/en.wikipedia/all-access/all-agents/".
		rawurlencode($wikiTitle).
		"/daily/$startDate/$endDate";
	curl_setopt($ch, CURLOPT_URL, $url);
	$json = json_decode(curl_exec($ch), true);
	$totalViews = 0;
	
	if( isset($json['items']) ) {
		foreach ( $json['items'] as $viewData ) {
			$totalViews += $viewData['views'];
		}
	} else {
		// Pageviews API returns null
		// echo "Error: ".$wikiTitle."\n";
	}
	$views = $json['items'][0]['views'];
	// TODO error handling
	return $totalViews/$daysBack;
}

