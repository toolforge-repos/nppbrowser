<?php

require_once __DIR__ . '/../common.php';

$moreThan = [
];

$moreThanOld = [
];

foreach ([50, 20, 10, 5, 2, 0] as $n) {
    $moreThan[$n] = [
        'count' => 0,
        'editors' => []
    ];

    $moreThanOld[$n] = [
        'count' => 0,
        'editors' => []
    ];
}

$byCount = $db->fetchAll('SELECT creator_name, count(*) as cnt, avg(length) as avlen FROM article WHERE redirect=0 AND patrol_status=0 GROUP BY creator_name ORDER BY cnt DESC');

$totalCount = 0;

foreach ($byCount as $r) {
    $u = $r['creator_name'];
    $count = $r['cnt'];

    $totalCount += $count;

    foreach ($moreThan as $threshold => &$table) {
        if ($count > $threshold) {
            $table['count'] += $count;
            $table['editors'][] = $u;
        }
    } unset($table);

    foreach ($moreThanOld as $threshold => &$table) {
        if ($count > $threshold) {
            $table['count'] += $count;
            $table['editors'][] = $u;
        }
    } unset($table);
}

echo "{| class='wikitable'
    |-
    ! Unreviewed Articles 
    ! Editors
    ! Articles
    ! Backlog fraction    
    ";

foreach ($moreThan as $threshold => $table) {
    echo "|-\n";

    if ($threshold == 0 ) {
        echo "| -  \n";
    } else {
        echo "| >" . $threshold . "\n";
    }

    echo "|" . count($table['editors']) . "\n";
    echo "|" . $table['count'] . "\n";
    echo "|" . round(($table['count']/$totalCount)*100) . "% \n";
}

echo "|- \n";
echo "| colspan=\"4\" | " . "<small>Generated: " . date('Y-m-d H:i:s', strtotime($db->fetchOne('SELECT max(creation_date) FROM stat'))) . " UTC</small>\n";

echo "|}\n\n";

echo "== Editors with most unreviewed articles ==\n\n";

echo "{| class='wikitable sortable'
    |-
    ! Editor
    ! Articles in backlog
    ! Avg page length
    ! Contributions (new articles)
    ! Random sample
    ";

foreach ($byCount as $r) {
    $u = $r['creator_name'];
    $count = $r['cnt'];

    if ($count <= 5) {
        continue;
    }

    echo "|-\n";

    echo "| [[User:" . htmlentities($u) . '|' . htmlentities($u) . ']]' . ' ([[User_talk:' . htmlentities($u) . '|talk]])' . "\n";
    echo "| $count \n";

    echo "| " . round($r['avlen']) . "\n";

    echo '| [' . 'https://en.wikipedia.org/w/index.php?limit=50&title=Special:Contributions&contribs=user&target=' . urlencode($u) . '&namespace=0&tagfilter=&newOnly=1&start=&end=' . ' contributions] ' . "\n";


    $sample = [];
    $available = range(0, $count - 1);

    while (count($sample) < 3) {
        $key = array_rand($available);
        $sample[] = $db->fetchRow('SELECT * FROM article WHERE creator_name=' . $db->quote($u) . ' AND redirect=0 AND patrol_status=0  LIMIT ' . $available[$key] . ', 1');
        unset($available[$key]);
    }

    echo "|\n";

    foreach ($sample as $s) {
        echo '* <small>[[' . htmlentities($s['title']) . ']] ' . str_replace(["\n","\r"], " ", $s['snippet']) . '</small>' . "\n";
    }
}

echo "|}";