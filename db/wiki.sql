-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 14, 2022 at 05:26 PM
-- Server version: 5.7.21-0ubuntu0.16.04.1
-- PHP Version: 7.0.18-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wiki`
--

-- --------------------------------------------------------

--
-- Table structure for table `article`
--
CREATE TABLE IF NOT EXISTS IF NOT EXISTS `article` (
  `id` int(10) UNSIGNED NOT NULL,
  `wiki_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `is_npp` boolean NOT NULL DEFAULT TRUE,
  `redirect` tinyint(1) NOT NULL DEFAULT '0',
  `category_count` int(10) NOT NULL,
  `linkcount` int(10) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `patrol_status` tinyint(1) NOT NULL DEFAULT '0',
  `revcount` int(10) NOT NULL,
  `afc_state` int,
  `length` int(10) NOT NULL,
  `snippet` text NOT NULL,
  `creator_name` varchar(255) NOT NULL,
  `creator_editcount` int(10) NOT NULL,
  `creator_autoconfirmed` int(10) NOT NULL,
  `creator_id` int(10) NOT NULL,
  `creation_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- --------------------------------------------------------
--
-- Table structure for table `article_new`
--

CREATE TABLE IF NOT EXISTS `article_new` (
  `id` int(10) UNSIGNED NOT NULL,
  `wiki_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `is_npp` boolean NOT NULL DEFAULT TRUE,
  `redirect` tinyint(1) NOT NULL DEFAULT '0',
  `category_count` int(10) NOT NULL,
  `linkcount` int(10) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `patrol_status` tinyint(1) NOT NULL DEFAULT '0',
  `revcount` int(10) NOT NULL,
  `afc_state` int,
  `length` int(10) NOT NULL,
  `snippet` text NOT NULL,
  `creator_name` varchar(255) NOT NULL,
  `creator_editcount` int(10) NOT NULL,
  `creator_autoconfirmed` int(10) NOT NULL,
  `creator_id` int(10) NOT NULL,
  `creation_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------
--
-- Table structure for table `block`
--

CREATE TABLE IF NOT EXISTS `block` (
  `id` int(12) UNSIGNED NOT NULL,
  `user` varchar(255) DEFAULT NULL,
  `userid` int(12) UNSIGNED DEFAULT NULL,
  `by` varchar(255) DEFAULT NULL,
  `byid` int(12) UNSIGNED DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `expiry` datetime DEFAULT NULL,
  `reason` text,
  `rangestart` varchar(255) DEFAULT NULL,
  `rangeend` varchar(255) DEFAULT NULL,
  `automatic` tinyint(1) NOT NULL DEFAULT '0',
  `anononly` tinyint(1) NOT NULL DEFAULT '0',
  `nocreate` tinyint(1) NOT NULL DEFAULT '0',
  `autoblock` tinyint(1) NOT NULL DEFAULT '0',
  `noemail` tinyint(1) NOT NULL DEFAULT '0',
  `allowusertalk` tinyint(1) NOT NULL DEFAULT '0',
  `hidden` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- --------------------------------------------------------
--
-- Table structure for table `cache`
--

CREATE TABLE IF NOT EXISTS `cache` (
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext CHARACTER SET utf8 NOT NULL,
  `creation_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------
--
-- Table structure for table `download`
--

CREATE TABLE IF NOT EXISTS `download` (
  `id` int(10) UNSIGNED NOT NULL,
  `url` varchar(1000) NOT NULL,
  `status` int(6) NOT NULL,
  `content` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------
--
-- Table structure for table `enwiki_newpage`
--

CREATE TABLE IF NOT EXISTS `enwiki_newpage` (
  `id` int(10) UNSIGNED NOT NULL,
  `month` int(2) NOT NULL,
  `year` int(4) NOT NULL,
  `daily` int(12) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------
--
-- Table structure for table `error`
--

CREATE TABLE IF NOT EXISTS `error` (
  `id` int(12) UNSIGNED NOT NULL,
  `operation` varchar(255) NOT NULL,
  `data` blob NOT NULL,
  `error` text NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------
--
-- Table structure for table `external_stat`
--

CREATE TABLE IF NOT EXISTS `external_stat` (
  `id` int(10) UNSIGNED NOT NULL,
  `domain` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `count` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------
--
-- Table structure for table `interlinking_cache`
--

CREATE TABLE IF NOT EXISTS `interlinking_cache` (
  `id` int(11) NOT NULL,
  `url` varbinary(500) NOT NULL,
  `api_response` longblob NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ts` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------
--
-- Table structure for table `log`
--

CREATE TABLE IF NOT EXISTS `log` (
  `id` int(10) UNSIGNED NOT NULL,
  `search` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sort_field` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sort_dir` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `start` int(10) NOT NULL,
  `length` int(10) NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------
--
-- Table structure for table `log_filtered`
--

CREATE TABLE IF NOT EXISTS `log_filtered` (
  `id` int(10) UNSIGNED NOT NULL,
  `search` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sort_field` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sort_dir` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `start` int(10) NOT NULL,
  `length` int(10) NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------
--
-- Table structure for table `paid_user`
--

CREATE TABLE IF NOT EXISTS `paid_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `sock_group` varchar(255) DEFAULT NULL,
  `editcount` int(10) UNSIGNED NOT NULL,
  `registration_date` datetime NOT NULL,
  `gender` varchar(255) NOT NULL,
  `paid_probability` int(10) UNSIGNED DEFAULT NULL,
  `user_page` longblob,
  `talk_page` longblob,
  `archive_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------
--
-- Table structure for table `pakistan`
--

CREATE TABLE IF NOT EXISTS `pakistan` (
  `id` int(12) UNSIGNED NOT NULL,
  `page` varchar(255) NOT NULL,
  `refcount` int(10) UNSIGNED NOT NULL,
  `reason` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------
--
-- Table structure for table `sqlcache`
--

CREATE TABLE IF NOT EXISTS `sqlcache` (
  `key` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `data` longblob NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------
--
-- Table structure for table `stat`
--

CREATE TABLE IF NOT EXISTS `stat` (
  `id` int(10) UNSIGNED NOT NULL,
  `count` int(10) NOT NULL,
  `autoconfirmed_count` int(10) NOT NULL,
  `median_user_editcount` float NOT NULL,
  `median_length` int(10) NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------
--
-- Table structure for table `_article_cache`
--

CREATE TABLE IF NOT EXISTS `_article_cache` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(500) NOT NULL,
  `content` longtext,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------
--
-- Table structure for table `_reference_cache`
--

CREATE TABLE IF NOT EXISTS `_reference_cache` (
  `id` int(10) UNSIGNED NOT NULL,
  `url` varchar(500) NOT NULL,
  `effective_url` varchar(500) DEFAULT NULL,
  `error` varchar(255) DEFAULT NULL,
  `status` int(6) DEFAULT NULL,
  `result` longtext,
  `content` longblob,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Indexes for dumped tables
--

--
-- Indexes for table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`),
  ADD KEY `title` (`title`),
  ADD KEY `category_count` (`category_count`),
  ADD KEY `linkcount` (`linkcount`),
  ADD KEY `status` (`status`),
  ADD KEY `patrol_status` (`patrol_status`),
  ADD KEY `revcount` (`revcount`),
  ADD KEY `length` (`length`),
  ADD KEY `creator_name` (`creator_name`),
  ADD KEY `creator_editcount` (`creator_editcount`),
  ADD KEY `creator_autoconfirmed` (`creator_autoconfirmed`),
  ADD KEY `redirect` (`redirect`),
  ADD KEY `creation_date` (`creation_date`);
ALTER TABLE `article` ADD FULLTEXT KEY `snippet` (`snippet`);

--
-- Indexes for table `article_new`
--
ALTER TABLE `article_new`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `block`
--
ALTER TABLE `block`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cache`
--
ALTER TABLE `cache`
  ADD PRIMARY KEY (`key`);

--
-- Indexes for table `download`
--
ALTER TABLE `download`
  ADD PRIMARY KEY (`id`),
  ADD KEY `url` (`url`);

--
-- Indexes for table `enwiki_newpage`
--
ALTER TABLE `enwiki_newpage`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `month` (`month`,`year`);

--
-- Indexes for table `error`
--
ALTER TABLE `error`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `external_stat`
--
ALTER TABLE `external_stat`
  ADD PRIMARY KEY (`id`),
  ADD KEY `count` (`count`),
  ADD KEY `domain` (`domain`);

--
-- Indexes for table `interlinking_cache`
--
ALTER TABLE `interlinking_cache`
  ADD PRIMARY KEY (`id`),
  ADD KEY `url` (`url`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_filtered`
--
ALTER TABLE `log_filtered`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paid_user`
--
ALTER TABLE `paid_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pakistan`
--
ALTER TABLE `pakistan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sqlcache`
--
ALTER TABLE `sqlcache`
  ADD PRIMARY KEY (`key`);

--
-- Indexes for table `stat`
--
ALTER TABLE `stat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `_article_cache`
--
ALTER TABLE `_article_cache`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `_reference_cache`
--
ALTER TABLE `_reference_cache`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `article`
--
ALTER TABLE `article`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64573;
--
-- AUTO_INCREMENT for table `article_new`
--
ALTER TABLE `article_new`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `download`
--
ALTER TABLE `download`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=179;
--
-- AUTO_INCREMENT for table `error`
--
ALTER TABLE `error`
  MODIFY `id` int(12) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `external_stat`
--
ALTER TABLE `external_stat`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1409544;
--
-- AUTO_INCREMENT for table `interlinking_cache`
--
ALTER TABLE `interlinking_cache`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13449;
--
-- AUTO_INCREMENT for table `log`
--
ALTER TABLE `log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63008;
--
-- AUTO_INCREMENT for table `log_filtered`
--
ALTER TABLE `log_filtered`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9489;
--
-- AUTO_INCREMENT for table `paid_user`
--
ALTER TABLE `paid_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=849;
--
-- AUTO_INCREMENT for table `pakistan`
--
ALTER TABLE `pakistan`
  MODIFY `id` int(12) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `stat`
--
ALTER TABLE `stat`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2560;
--
-- AUTO_INCREMENT for table `_article_cache`
--
ALTER TABLE `_article_cache`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `_reference_cache`
--
ALTER TABLE `_reference_cache`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=963;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
