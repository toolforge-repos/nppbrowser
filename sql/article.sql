-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 03, 2017 at 12:59 AM
-- Server version: 5.7.16-0ubuntu0.16.04.1
-- PHP Version: 7.0.18-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wiki`
--

-- --------------------------------------------------------

--
-- Table structure for table `article_new`
--

DROP TABLE IF EXISTS article_new;

CREATE TABLE `article_new` (
  `id` int(10) UNSIGNED NOT NULL,
  `wiki_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_general_ci NOT NULL,
  `is_npp` boolean NOT NULL DEFAULT false,
  `redirect` tinyint(1) NOT NULL DEFAULT '0',
  `category_count` int(10) NOT NULL,
  `linkcount` int(10) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `patrol_status` tinyint(1) NOT NULL DEFAULT '0',
  `revcount` int(10) NOT NULL,
  `afc_state` int,
  `length` int(10) NOT NULL,
  `snippet` text COLLATE utf8_general_ci NOT NULL,
  `creator_name` varchar(255) COLLATE utf8_general_ci NOT NULL,
  `creator_editcount` int(10) NOT NULL,
  `creator_autoconfirmed` int(10) NOT NULL,
  `creator_id` int(10) NOT NULL,
  `creation_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

ALTER TABLE `article_new`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `article_new`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `article_new`
--
ALTER TABLE `article_new`
  ADD KEY `title` (`title`),
  ADD KEY `category_count` (`category_count`),
  ADD KEY `linkcount` (`linkcount`),
  ADD KEY `status` (`status`),
  ADD KEY `patrol_status` (`patrol_status`),
  ADD KEY `revcount` (`revcount`),
  ADD KEY `length` (`length`),
  ADD KEY `creator_name` (`creator_name`),
  ADD KEY `creator_editcount` (`creator_editcount`),
  ADD KEY `creator_autoconfirmed` (`creator_autoconfirmed`),
  ADD KEY `redirect` (`redirect`),
  ADD KEY `creation_date` (`creation_date`);
ALTER TABLE `article_new` ADD FULLTEXT KEY `snippet` (`snippet`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `article_new`
--

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


DROP TABLE article;
RENAME TABLE article_new TO article;

TRUNCATE TABLE cache;
