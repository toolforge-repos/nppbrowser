<?php

$result = getArticlesByGroups($groups, $mode);

?>

<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>NPP Browser</title>

    <!-- Bootstrap Core CSS -->
    <link href="./vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="./vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- DataTables CSS -->
    <link href="./vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="./vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="./dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="./vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="./vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="./">NPP Browser <small style="font-size:0.5em">v. 1.3.0</small></a>
        </div>
        <!-- /.navbar-header -->

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li class="sidebar-search">
                        <div class="input-group custom-search-form">

                            </span>
                        </div>

                    </li>
                    <li>
                        <?php require '../includes/menu.php'; ?>
                    </li>
                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                &nbsp;
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <div class="row">
            <div class="col-lg-12">
                <button type="button" class="btn btn-default"><a href="index.php?by=group&mode=<?=$mode?>&name=<?= $chosenGroup; ?>&format=wiki">Export as WikiTable</a></button>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="panel-group" id="accordion">
                    <?php foreach($result as $g => $arts): ?>
                        <h3><?= $g; ?></h3>
                        <?php foreach($arts as $k => $articles): ?>
                            <div class="panel panel-default">

                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" style="color:blue;text-decoration:underline" data-parent="#accordion" href="#collapse<?= str_replace(" ","",$k);?>" aria-expanded="false" class="collapsed"><?= trim($k); ?></a> (<?= count($articles); ?>)
                                    </h4>
                                </div>

                                <div id="collapse<?= str_replace(" ","",$k);?>" <?php if(count($articles) > 25 && (count($arts) > 1 || count($result) > 1)): ?>class="panel-collapse collapse" aria-expanded="false" style="height: 0px;"<?php else:?>class="panel-collapse collapse in" aria-expanded="true"<?php endif;?>>
                                    <div class="panel-body">
                                        <div class="row">
                                            <?php for($i=0;$i<4;$i++): ?>
                                                <div class="fa col-lg-3">
                                                    <?php for($j=$i;$j<count($articles);$j+=4): ?>
                                                        <?php

                                                        $icon = 'b';
                                                        $title = '';

                                                        if ($articles[$j]['creator_editcount'] > 5000) {
                                                            //$icon = 'glyphicon-sunglasses';
                                                            //$title = 'Creator edit count > 5000';
                                                        }

                                                        if ( $articles[$j]['redirect'] ) {
                                                            $icon = 'fa-arrow-circle-o-right';
                                                            $title = 'Redirect';
                                                        } elseif ($articles[$j]['linkcount'] < 2) {
                                                            $icon = 'fa-unlink';
                                                            $title = 'Link count < 2';
                                                        }

                                                        if ($articles[$j]['creator_editcount'] < 50) {
                                                            $icon = 'fa-user';
                                                            $title = 'Creator edit count < 50';
                                                        }

                                                        if ($articles[$j]['status']) {
                                                            $icon = 'fa-trash-o';
                                                            $title = 'Nominated for deletion';
                                                        }

                                                        ?>
                                                        <p class="fa <?= $icon ?>" <?php if($icon): ?> title="<?= $title;?>"<?php endif;?> > <a style="color:blue" target="_blank" href="https://en.wikipedia.org/wiki/<?= htmlentities($articles[$j]['title']);?><?= $articles[$j]['redirect'] ? "?redirect=no" : ""; ?>"><?= htmlentities($articles[$j]['title']); ?></a> </p>
                                                        <br>
                                                    <?php endfor; ?>
                                                </div>
                                            <?php endfor; ?>
                                        </div>
                                        <!-- /.row (nested) -->
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>

                    <?php endforeach; ?>
                </div>
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="./vendor/jquery/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="./vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="./vendor/metisMenu/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript -->
<script src="./vendor/raphael/raphael.min.js"></script>

<!-- DataTables JavaScript -->
<script src="./vendor/datatables/js/jquery.dataTables.min.js"></script>
<script src="./vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
<script src="./vendor/datatables-responsive/dataTables.responsive.js"></script>

<!-- Custom Theme JavaScript -->
<script src="./dist/js/sb-admin-2.js"></script>

<script src="./js/browse.js"></script>


</body>

</html>


